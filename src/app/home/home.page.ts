import { Component, OnInit } from '@angular/core';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { GooglePlus } from '@ionic-native/google-plus/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{
  user: any = {};
  constructor(private http: HttpClient, private fb: Facebook, private goog: GooglePlus) {}

  ngOnInit(){
  }
////https://github.com/arnesson/cordova-plugin-firebase/issues/1081
  aut_facebook() {
    this.fb.login(['public_profile','email']) 
      .then((res: FacebookLoginResponse) => {
        if (res.status === 'connected') {
          this.user.img = 'https://graph.facebook.com/' + res.authResponse.userID + '/picture?type=square';
          this.getToken(res.authResponse.accessToken);
        } else {
          console.log("Error");
        }
      })
      .catch((e)=> {    //Si no a iniciado sesion en facebook entra al catch y vueve a llamar la funcion para que se logee (asi es como trate el error)
        if(e.errorCode ==="4201" && e.errorMessage==="User cancelled dialog"){
          this.aut_facebook();
        }else{
          console.log("Error de conexion");
        }
      });
  }
  getToken(access_token: string) {
    let url = 'https://graph.facebook.com/me?fields=id,name,first_name,last_name,email&access_token=' + access_token;
    this.http.get(url).subscribe(data => {
      console.log("Datos: ",JSON.stringify(data));
    })
  }


  salir_facebook(){
    this.fb.logout().then((data) => {
      console.log(data);
    }).catch((res) =>  {
      console.log(res);
    });
  }
  sesion_activa_facebook(){
    this.fb.getLoginStatus().then((data)=>{
      console.log(data);
    }).catch((res)=>{
      console.log(res);
    })
  }
  
  aut_google(){
    this.goog.login({})
    .then(response => {
      console.log(JSON.stringify(response));
    })
    .catch(err => console.error(err));
  }

  salir_google(){
    this.goog.logout().then((data) => {
      console.log(data);
    }).catch((res) =>  {
      console.log(res);
    });
  }

}
